package chinalife.com.tw.pdfpoc;

/**
 * Created by Leo.chang on 2018/11/8.
 */

public class PdfConfig {
    public final static String copyright = "Ⓒ Chinalife Insurance Co., Ltc.";
    public final static int A4_width = 595;
    public final static int A4_height = 841;
}
