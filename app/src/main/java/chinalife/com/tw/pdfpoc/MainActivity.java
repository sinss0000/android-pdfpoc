package chinalife.com.tw.pdfpoc;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.graphics.pdf.PdfDocument.*;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_export).setOnClickListener(onExportClick);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 9001) {
            //Permission granted
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 9001);
        }
        //小於5.0，不理會
        return true;
    }

    private void generatePDF() {
        if (checkPermissions() == false) {
            Toast.makeText(this, "缺少必要的權限", Toast.LENGTH_LONG).show();
            return;
        }
        long start = new Date().getTime();
        PdfDocument document = new PdfDocument();

        for (int i = 0; i < 10; i++) {
            PageInfo.Builder pb = new PageInfo.Builder(PdfConfig.A4_width, PdfConfig.A4_height, 1);
            PageInfo pageInfo = pb.create();
            Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();
            Paint paint = new Paint();
            paint.setTextSize(20);
            canvas.translate(10, 10);
            Rect logoRect = new Rect();
            logoRect.set(20, 20, 150, 51);
            Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            logo = Bitmap.createScaledBitmap(logo, 150, 51, true);
            canvas.drawBitmap(logo, null, logoRect, paint);

            TextPaint txt = new TextPaint();
            txt.setColor(Color.BLACK);
            txt.setTextSize(16);
            txt.setTextAlign(Paint.Align.LEFT);
            Typeface typeface = Typeface.create(Typeface.MONOSPACE, Typeface.NORMAL);
            txt.setTypeface(typeface);

            canvas.translate(10, 400);
            String content = i + ": 山不在高，有仙則名；水不在深，有龍則靈。斯是陋室，惟吾德馨詞解。苔痕上階綠，草色入簾青；談笑有鴻儒詞解，往來無白丁詞解。可以調素琴詞解，閱金經；無絲竹之亂耳，無案牘詞解之勞形詞解。南陽諸葛廬詞解，西蜀子雲亭詞解。孔子云：「何陋之有詞解？」";
            StaticLayout txtLayout = new StaticLayout(content, txt, PdfConfig.A4_width - 20,
            Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            txtLayout.draw(canvas);
            document.finishPage(page);
        }

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/chinalife.pdf";

        File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(file);
            document.writeTo(outputStream);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.close();
        long end = new Date().getTime();
        Log.d("chinalife", "cost time =" + (end - start) / 1000.0f + "s");

        Log.d("TAG", path);
    }

    View.OnClickListener onExportClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            generatePDF();
        }
    };
}
